//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2020 Lorenzo La Mura

#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {INTEGER , BOOLEAN, CHAR, DOUBLE};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
map<string, enum TYPES> DeclaredVariables;
unsigned long TagNumber=0;

string* var=new string[255];// Pour	recuperer le nom de la variable ( par exemple dans le for ). On fait un Tableau pour faire des for imbriqués
int n=0;//Longeuer du Tableau
TYPES* typ=new TYPES[255];//Deuxieme tableau pour comparer le type des deux expression dans le for for exemple
int nb=0;//Longuer de ce dernier Tableau
string* fonction=new string[255];//Tableau pour avoir le nom de toutes les procedures appellées
int x=0;//Longuer de ce tableau
string** array=new string*[255];//Tableau pour contenir tous les arrays qui seront declarés au debut du program(comme on fait en pascal)
//Le 1ere element (cad array[i][0]) sera le nom de l'array, les autres ses elements, l'avant dernier(ou dernier) son type(cad array[i][indice_type[i]-1]=INTIGER par exemple)
//Le dernier element sera le nom de la variable qui contient cette array si elle existe(par exemple en faisant a:=nom_array;)
//Chaque array pourra etre affecté à une seule variable pour comoditè et faciliter l'utilise des indices du tableau array
//Mais on peut affecter la valeur d'un array à traver son indice à plusiers variables et donc faire aussi des expression.
//Pour avoir le 1er element d'un array son indice sera donc 1 et non pas 0
int* indice_type=new int[255];//Tableau qui contient l'indice du type d'un array i
int y1=0;//Premier dimension du tableau array
int y2=0;//2eme dimesion du tableau(où il y aura tous les information pour chaque array crée)
int y3=0;//Utilisé par Factor et AssignementStatement pour verifier si on veut affecter une array à une variable.
int y4=0;//indice i du Tableau_Array (voir Factor)

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

void Statement(void);
void StatementPart(void);
enum TYPES SqrFonction(void);//Called by Factor
enum TYPES SinFonction(void);//Called by Factor
enum TYPES CosFonction(void);//Called by Factor
enum TYPES SqrtFonction(void);//Called by Factor
enum TYPES AbsFonction(void);//Called by Factor
enum TYPES MaxFonction(void);//Called by Factor
enum TYPES MinFonction(void);//Called by Factor
enum TYPES SuccFonction(void);//Called by Factor
enum TYPES PredFonction(void);//Called by Factor
enum TYPES LengthFonction(void);//Called by Factor
enum TYPES FactorialFonction(void);//Called by Factor
enum TYPES PowerFonction(void);//Called by Factor
		
enum TYPES Identifier(void){	
	enum TYPES type;
	if(!IsDeclared(lexer->YYText())){
		cerr<<"Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
	}
	type=DeclaredVariables[lexer->YYText()];
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return type;
}

enum TYPES Number(void){
	bool is_a_decimal=false;
	double d;					// 64-bit float
	unsigned int *i;			// pointer to a 32 bit unsigned int 
	string	number=lexer->YYText();
	if(number.find(".")!=string::npos)
	{
		d=atof(lexer->YYText());
		i=(unsigned int *) &d; 
		cout <<"\tsubq $8,%rsp\t\t\t# allocate 8 bytes on stack's top"<<endl;
		cout <<"\tmovl	$"<<*i<<", (%rsp)\t# Conversion of "<<d<<" (32 bit high part)"<<endl;
		cout <<"\tmovl	$"<<*(i+1)<<", 4(%rsp)\t# Conversion of "<<d<<" (32 bit low part)"<<endl;
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else
	{ 
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
}

enum TYPES CharConst(void){
	cout<<"\tmovq $0, %rax"<<endl;
	cout<<"\tmovb $"<<lexer->YYText()<<",%al"<<endl;
	cout<<"\tpush %rax\t# push a 64-bit version of "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return CHAR;
}


enum TYPES Expression(void);			// Called by Term() and calls Term()

enum TYPES ArrayIndice(int i,int b){
	current=(TOKEN) lexer->yylex();
	if(current!=NUMBER) Error("Un numero était attendu !");
	string indice=lexer->YYText();
	int ind=atoi(indice.c_str());
	if(array[i][b]!="INTEGER" and array[i][b]!="DOUBLE") Error("Array dejà utilisé par une autre variable! ");
	if(ind<=0 or ind>=b) Error("Indice Introuvable ! ");
	current=(TOKEN) lexer->yylex();
	if(current!=LBRACKET) Error("']' était attendu !");
	current=(TOKEN) lexer->yylex();
	if(array[i][b]=="INTEGER"){
		cout<<"\tpush $"<<array[i][ind]<<endl;
		y3=0;
		y4=0;
		return INTEGER;
	}
	else if(array[i][b]=="DOUBLE"){
		unsigned int* ind2;
		double d;
		string indice=array[i][ind];	
		d=atof(indice.c_str());
		ind2=(unsigned int *) &d; 
		cout <<"\tsubq $8,%rsp\t\t\t# allocate 8 bytes on stack's top"<<endl;
		cout <<"\tmovl	$"<<*ind2<<", (%rsp)\t# Conversion of "<<d<<" (32 bit high part)"<<endl;
		cout <<"\tmovl	$"<<*(ind2+1)<<", 4(%rsp)\t# Conversion of "<<d<<" (32 bit low part)"<<endl;
		y3=0;
		y4=0;
		return DOUBLE;
	}
	else
		Error("Type incorrecte ou tu cherche d'affecter une array à deux variable differentes. Impossible de Continuer! ");
}

//Facteur := ..... | ARRAY {ArrayIndice} | SQR (< Expression >) | SIN (<Expression) | COS (<Expression) | SQRT (<Expression) | ABS (<Expression) | MAX (<Expression) | MIN (<Expression)    
enum TYPES Factor(void){
	TYPES type; 
	//On verifie si on veut affeter à la variable un array entier(mais un ce cas on pourra faire seulement un DISPLAY et pas d'autres fonctions car on fera pas de pop dans l'AssignementStatement) ou in indice précis d'un array(en ce cas on faire un push et on pourra donc faire un expression mais il faudra separer le + du [).
	for(int i=0;i<y1;i++){
		if(!strcmp(lexer->YYText(),array[i][0].c_str())){
			int b=indice_type[i];
			current=(TOKEN) lexer->yylex();
			if(current==RBRACKET){//On veut affecter un indice précis.
				type=ArrayIndice(i,b);
				return type;
			}
			else if(current!=SEMICOLON) Error(" ';' était attendu. On peut pas faire une expression avec un array entier . ");
			y3=1;//On recconait comme ça qu'on est passé par la et que on veut bien affecter un array entier à une variable;
			y4=i;//valeur de i pour enserer la variable dedans dans l'AssignementStatement. Comme ça on peut savoir si une variable contient un array.
			if(array[i][b]=="INTEGER"){
				indice_type[i]++;
				return INTEGER;
			}
			else if(array[i][b]=="DOUBLE"){
				indice_type[i]++;
				return DOUBLE;
			}
			else
				cerr<<array[i][b];
				Error("Type incorrecte ou tu cherche d'affecter une array à deux variable differentes. Impossible de Continuer! ");
		}
		int b=indice_type[i];
		if(!strcmp(lexer->YYText(),array[i][b].c_str())) Error("On peut pas utiliser la valeure d'un array entier à travers son indice. Avec cette array est possible seulement l'affichage ! "); 
	}		
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type=Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		
		else
			current=(TOKEN) lexer->yylex();
	}	
	else {
		if (current==NUMBER)
		{	
			type=Number();
		}
	    else if(current==ID)
		{
				type=Identifier();
		}
		else if(current==CHARCONST)
		{
			type=CharConst();
		}
		else if(current==KEYWORD)
		{
			if(!strcmp(lexer->YYText(),"SQR"))
				type=SqrFonction();
			else if(!strcmp(lexer->YYText(),"SIN"))
				type=SinFonction();
			else if(!strcmp(lexer->YYText(),"COS"))
				type=CosFonction();
			else if(!strcmp(lexer->YYText(),"SQRT"))
				type=SqrtFonction();
			else if(!strcmp(lexer->YYText(),"ABS"))
				type=AbsFonction();
			else if(!strcmp(lexer->YYText(),"MAX"))
				type=MaxFonction();
			else if(!strcmp(lexer->YYText(),"MIN"))
				type=MinFonction();
			else if(!strcmp(lexer->YYText(),"SUCC"))
				type=SuccFonction();
			else if(!strcmp(lexer->YYText(),"PRED"))
				type=PredFonction();
			else if(!strcmp(lexer->YYText(),"LENGTH"))
				type=LengthFonction();
			else if(!strcmp(lexer->YYText(),"FACT"))
				type=FactorialFonction();
			else if(!strcmp(lexer->YYText(),"POW"))
				type=PowerFonction();
			else Error("'(' ou chiffre ou lettre attendue");
		}
		else Error("'(' ou chiffre ou lettre attendue");
	}
	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
enum TYPES Term(void){
	TYPES type1, type2;
	OPMUL mulop;
	type1=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type2=Factor();
		if(type2!=type1)
			Error("types incompatibles dans l'expression");
		switch(mulop){
			case AND:
				if(type2!=BOOLEAN)
					Error("type non booléen pour l'opérateur AND");
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				if(type2!=INTEGER&&type2!=DOUBLE)
					Error("type non numérique pour la multiplication");
				if(type2==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL"<<endl;	// store result
				}
				else{
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfmulp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;
			case DIV:
				if(type2!=INTEGER&&type2!=DOUBLE)
					Error("type non numérique pour la division");
				if(type2==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result
				}
				else{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfdivp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;
			case MOD:
				if(type2!=INTEGER)
					Error("type non entier pour le modulo");
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
enum TYPES SimpleExpression(void){
	enum TYPES type1, type2;
	OPADD adop;
	type1=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		type2=Term();
		if(type2!=type1)
			Error("types incompatibles dans l'expression");
		switch(adop){
			case OR:
				if(type2!=BOOLEAN)
					Error("opérande non booléenne pour l'opérateur OR");
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\torq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				cout << "\tpush %rax"<<endl;			// store result
				break;			
			case ADD:
				if(type2!=INTEGER&&type2!=DOUBLE)
					Error("opérande non numérique pour l'addition");
				if(type2==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					cout << "\tpush %rax"<<endl;			// store result
				}
				else{
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfaddp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;			
			case SUB:	
				if(type2!=INTEGER&&type2!=DOUBLE)
					Error("opérande non numérique pour la soustraction");
				if(type2==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tsubq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					cout << "\tpush %rax"<<endl;			// store result
				}
				else{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfsubp	%st(0),%st(1)\t# %st(0) <- op1 - op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;	
			default:
				Error("opérateur additif inconnu");
		}
	}
	return type1;
}

//Type := return le type 
enum TYPES Type(void){
	if(current!=KEYWORD) Error("type attendu");
	else{
		if(strcmp(lexer->YYText(),"BOOLEAN")==0)
		{
			current=(TOKEN) lexer->yylex();
			return BOOLEAN;
		}	
		else if(strcmp(lexer->YYText(),"INTEGER")==0)
		{
			current=(TOKEN) lexer->yylex();
			return INTEGER;
		}
		else if(strcmp(lexer->YYText(),"DOUBLE")==0)
		{
			current=(TOKEN) lexer->yylex();
			return DOUBLE;
		}
		else if(strcmp(lexer->YYText(),"CHAR")==0)
		{
			current=(TOKEN) lexer->yylex();
			return CHAR;
		}
		else Error("type inconnu");
	}
}

// VarDeclaration := Ident {"," Ident} ":" Type
void VarDeclaration(void){
	set<string> idents;
	enum TYPES type;
	if(current!=ID) Error("Un identificater était attendu !");
	idents.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA)
	{
		current=(TOKEN) lexer->yylex();
		if(current!=ID) Error("Un identificateur était attendu !");
		idents.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=COLON) Error("caractère ':' attendu");
	current=(TOKEN) lexer->yylex();
	type=Type();
	for (set<string>::iterator it=idents.begin(); it!=idents.end(); ++it){
		switch(type){
			case BOOLEAN:
			case INTEGER:
				cout << *it << ":\t.quad 0"<<endl;
				break;
			case DOUBLE:
				cout << *it << ":\t.double 0.0"<<endl;
				break;
			case CHAR:
				cout << *it << ":\t.byte 0"<<endl;
				break;
			default:
				Error("type inconnu.");
		};
		DeclaredVariables[*it]=type;
	}
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void){
	current=(TOKEN) lexer->yylex();
	VarDeclaration();
	while(current==SEMICOLON)
	{
		current=(TOKEN) lexer->yylex();
		VarDeclaration();
	}
	if(current!=DOT)
		Error("'.' attendu");
	current=(TOKEN) lexer->yylex();
}

// ArrayDeclaration := Ident {"," Ident} ":" Type
void ArrayDeclaration(void){
	string element_array;
	enum TYPES type1,type2;
	current=(TOKEN) lexer->yylex();
	if(!strcmp(lexer->YYText(),"ARRAY")){
		current=(TOKEN) lexer->yylex();
		if(current!=RBRACKET)
			Error("'[' était attendu ! ");
		current=(TOKEN) lexer->yylex();
		if(current!=NUMBER) Error("Un numero était attendu !");
		element_array=lexer->YYText();
		array[y1-1][y2]=element_array;
		y2++;
		type1=Expression();
		while(current==COMMA)
		{
			current=(TOKEN) lexer->yylex();
			if(current!=NUMBER) Error("Un numero était attendu !");
			element_array=lexer->YYText();
			array[y1-1][y2]=element_array;
			y2++;
			type2=Expression();
			if(type1!=type2) Error("Meme type attendu ! ");
		}
		if(current!=LBRACKET)
			Error("']' était attendu ! ");
		current=(TOKEN) lexer->yylex();
		if(current!=KEYWORD and strcmp(lexer->YYText(),"OF")!=0) Error("OF attendu ! ");
		current=(TOKEN) lexer->yylex();
		if(!strcmp(lexer->YYText(),"INTEGER")){
			type2=INTEGER;
			if(type1!=type2) Error("Meme type attendu ! ");
			current=(TOKEN) lexer->yylex();
			element_array="INTEGER";
			array[y1-1][y2]=element_array;
			indice_type[y1-1]=y2;
			y2=0;
			y1++;
		}
		else if(!strcmp(lexer->YYText(),"DOUBLE")){
			type2=DOUBLE;
			if(type1!=type2) Error("Meme type attendu ! ");
			current=(TOKEN) lexer->yylex();
			element_array="DOUBLE";
			array[y1-1][y2]=element_array;
			indice_type[y1-1]=y2;
			y2=0;
			y1++;
		}
		//Avec les char il y avait un probléme de segmentation alors je prefére les enlever
		else Error(" Seulement le Type INTIGER ou DOUBLE sont acceptés pour ce Array .");
	}
}

// TypeDeclarationPart := "TYPE" ArrayDeclaration
void TypeDeclaration(void){
	string nom_array;
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
			Error(" Un ID était attendu ! ");
	nom_array=lexer->YYText();
	array[y1][y2]=nom_array;
	y1++;
	y2++;
	current=(TOKEN) lexer->yylex();
	if(current!=EGAL)
		Error(" '=' était attendu ! ");//L'array sera declaré avec un egal ex: lol = ARRAY[....] (comme en pascal); avec un point pour terminer et passer au VarDeclaration
	ArrayDeclaration();
}
	
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
enum TYPES Expression(void){
	enum TYPES type1, type2;
	unsigned long long tag;
	OPREL oprel;
	type1=SimpleExpression();
	if(current==RELOP){
		tag=++TagNumber;
		oprel=RelationalOperator();
		type2=SimpleExpression();
		if(type2!=type1)
			Error("types incompatibles pour la comparaison");
		if(type1!=DOUBLE){
			cout << "\tpop %rax"<<endl;
			cout << "\tpop %rbx"<<endl;
			cout << "\tcmpq %rax, %rbx"<<endl;
		}
		else{
			cout<<"\tfldl	(%rsp)\t"<<endl;
			cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
			cout<<"\t addq $16, %rsp\t# 2x pop nothing"<<endl;
			cout<<"\tfcomip %st(1)\t\t# compare op1 and op2 -> %RFLAGS and pop"<<endl;
			cout<<"\tfaddp %st(1)\t# pop nothing"<<endl;
		}
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<tag<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<tag<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<tag<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<tag<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<tag<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<tag<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<tag<<endl;
		cout << "Vrai"<<tag<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<tag<<":"<<endl;
		return BOOLEAN;
	}
	return type1;
}

//PowerFonction:= '(' <Expression> ',' <Expression> ')'
enum TYPES PowerFonction(void){
	unsigned long long tag=TagNumber++;
	current=(TOKEN) lexer->yylex();
	if(current!=RPARENT)
			Error("'(' était attendu ! ");
	current=(TOKEN) lexer->yylex();
	TYPES type=Expression();
	if (type==INTEGER){
		cout<<"\tpop POW_INTEGER"<<endl;
		if(current!=COMMA)
			Error("',' était attendu ! ");
		current=(TOKEN) lexer->yylex();
		TYPES type2=Expression();
		if (type2!=INTEGER) Error("L'exposant dois etre un INTEGER ! ");
		cout<<"\tpop %rbx"<<endl;
		cout<<"\tpush POW_INTEGER"<<endl;
		cout<<"\tpop %rax"<<endl;
		cout<<"\tpush POW_INTEGER"<<endl;
		cout<<"\tpop %rcx"<<endl;
		
		cout<<"Power"<<tag<<" : "<<endl;
		cout<<"\tsubq $1, %rbx"<<endl;
		
		cout<<"\tcmpq $1,%rbx"<<endl;
		cout<<"\tjnae endPower"<<tag<<endl;
		cout<<"\tmulq %rcx"<<endl;	// a * b -> %rdx:%rbx
		cout<<"\tjmp Power"<<tag<<endl;
		cout<<"endPower"<<tag<<" : "<<endl;
		cout<<"\tpush %rax"<<endl;
		if(current!=LPARENT)
				Error("')' était attendu ! ");
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
	else if(type==DOUBLE){
		cout<<"\tpop POW_INTEGER"<<endl;
		if(current!=COMMA)
			Error("',' était attendu ! ");
		current=(TOKEN) lexer->yylex();
		TYPES type2=Expression();
		if (type2!=INTEGER) Error("L'exposant dois etre un INTEGER ! ");
		cout<<"\tpop %rbx"<<endl;
		cout<<"\tpush POW_INTEGER"<<endl;
		cout<<"\tpop %rax"<<endl;
		cout<<"\tpush POW_INTEGER"<<endl;
		cout<<"\tpop %rcx"<<endl;
		
		cout<<"Power"<<tag<<" : "<<endl;
		cout<<"\tsubq $1, %rbx"<<endl;
		
		cout<<"\tcmpq $1,%rbx"<<endl;
		cout<<"\tjnae endPower"<<tag<<endl;
		
		cout<<"\tpush %rax"<<endl;
		cout<<"\tpush %rcx"<<endl;
		cout<<"\tfldl	8(%rsp)\t"<<endl;
		cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
		cout<<"\tfmulp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
		cout<<"\tfstpl 8(%rsp)"<<endl;
		cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl;
		cout<<"\tpop %rax"<<endl;
		 
		cout<<"\tjmp Power"<<tag<<endl;
		cout<<"endPower"<<tag<<" : "<<endl;
		cout<<"\tpush %rax"<<endl;
		if(current!=LPARENT)
				Error("')' était attendu ! ");
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}				
	else Error("Power necessite le type INTEGER ou DOUBLE ! ");
}

//FactorialFonction:= '(' <Expression> ')'
enum TYPES FactorialFonction(void){
	unsigned long long tag=TagNumber++;
	current=(TOKEN) lexer->yylex();
	if(current!=RPARENT)
			Error("'(' était attendu ! ");
	current=(TOKEN) lexer->yylex();
	TYPES type=Expression();
	if (type!=INTEGER) Error("Type INTEGER obligatoire Pour le factorielle ! ");
	cout<<"\tpop %rbx"<<endl;//n-1
	cout<<"\tpush %rbx"<<endl;
	cout<<"\tpop %rax"<<endl;//n
	
	cout<<"Factorial"<<tag<<" : "<<endl;
	cout<<"\tsubq $1, %rbx"<<endl;
	
	cout<<"\tmulq %rbx"<<endl;	// a * b -> %rdx:%rbx
	cout<<"\tcmpq $1,%rbx"<<endl;
	cout<<"\tje endFactorial"<<tag<<endl;
	cout<<"\tjmp Factorial"<<tag<<endl;
	cout<<"endFactorial"<<tag<<" : "<<endl;
	cout<<"\tpush %rax"<<endl;
	if(current!=LPARENT)
			Error("')' était attendu ! ");
	current=(TOKEN) lexer->yylex();
	return INTEGER;
}

enum TYPES LengthFonction(void){
	int a=0;
	current=(TOKEN) lexer->yylex();
	if(current!=RPARENT)
			Error("'(' était attendu ! ");
	current=(TOKEN) lexer->yylex();
	for(int i=0;i<y1;i++){
		for(int t=0;t<255;t++){
			if(!strcmp(lexer->YYText(),array[i][t].c_str())){
				a++;
				int b=indice_type[i];
				if(array[i][b]!="INTEGER" and array[i][b]!="DOUBLE") cout<<"\tpush $"<<b-2<<endl;//On fait moins deux pour enlever le premier indice et le dernier et le type
				else cout<<"\tpush $"<<b-1<<endl;//on enleve que le premier indice et le type car dans le Tableau il'y a pas une variable à la quelle le array a été affecté
			}
		}
	}
	if(a==0) Error("Cet array n'existe pas ! ");
	current=(TOKEN) lexer->yylex();
	if(current!=LPARENT)
			Error("')' était attendu ! ");
	current=(TOKEN) lexer->yylex();
	return INTEGER;
}

// PredFonction := Expression
enum TYPES PredFonction(void){
	if(!strcmp(lexer->YYText(),"PRED")){
		current=(TOKEN) lexer->yylex();
		if(current!=RPARENT)
			Error("'(' était attendu ! ");
		current=(TOKEN) lexer->yylex();
		TYPES type = Expression(); 
		if(type==DOUBLE){		
			cout<<"\tpush FOR_DOUBLE \t# Pour enlever 1.0"<<endl;	
			cout<<"\tfldl	8(%rsp)\t"<<endl;
			cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
			cout<<"\tfsubp	%st(0),%st(1)\t# %st(0) <- op1 - op2 ; %st(1)=null"<<endl;
			cout<<"\tfabs "<<endl;//car il fait '1 - la variable'
			cout<<"\tfstpl 8(%rsp)"<<endl;
			cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl;
			if(current!=LPARENT)
				Error("')' était attendu !");
			current=(TOKEN) lexer->yylex();
			return type;
		}
		if(type==INTEGER){
			cout<<"\tpop SQR_INTEGER \t# Pour inserer dans une variable le pop de l'expression"<<endl;
			cout<<"\tsubq $1,SQR_INTEGER"<<endl;
			cout<<"\tpush SQR_INTEGER"<<endl;
			if(current!=LPARENT)
				Error("')' était attendu !");
			current=(TOKEN) lexer->yylex();
			return type;
		}
		else
			Error("La Fonction PRED nécessite le type DOUBLE ou INTEGER ! ");
	}	
	else
		Error("PRED était attendu . ");
}
	
// SuccFonction := Expression
enum TYPES SuccFonction(void){
	if(!strcmp(lexer->YYText(),"SUCC")){
		current=(TOKEN) lexer->yylex();
		if(current!=RPARENT)
			Error("'(' était attendu ! ");
		current=(TOKEN) lexer->yylex();
		TYPES type = Expression(); 
		if(type==DOUBLE){		
			cout<<"\tpush FOR_DOUBLE \t# Pour rajouter 1.0"<<endl;	
			cout<<"\tfldl	8(%rsp)\t"<<endl;
			cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
			cout<<"\tfaddp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
			cout<<"\tfstpl 8(%rsp)"<<endl;
			cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl;
			if(current!=LPARENT)
				Error("')' était attendu !");
			current=(TOKEN) lexer->yylex();
			return type;
		}
		if(type==INTEGER){
			cout<<"\tpop SQR_INTEGER \t# Pour inserer dans une variable le pop de l'expression"<<endl;
			cout<<"\taddq $1,SQR_INTEGER"<<endl;
			cout<<"\tpush SQR_INTEGER"<<endl;
			if(current!=LPARENT)
				Error("')' était attendu !");
			current=(TOKEN) lexer->yylex();
			return type;
		}
		else
			Error("La Fonction SUCC nécessite le type DOUBLE ou INTEGER ! ");
	}	
	else
		Error("SUCC était attendu . ");
}

// MaxFonction := Expression { Expression; }
enum TYPES MaxFonction(void){
	unsigned long long tag=TagNumber++;
	if(!strcmp(lexer->YYText(),"MAX")){
		cout<<"Maxinit"<<tag<<" : "<<endl;
		current=(TOKEN) lexer->yylex();
		if(current!=RPARENT)
			Error("'(' était Attendu ! ");
		current=(TOKEN) lexer->yylex();
		TYPES type1 = Expression(); 
		cout<<"\tpop %rdx"<<endl;
		if(type1==INTEGER){
			if(current!=COMMA)
				Error("',' était Attendu ! ");
			current=(TOKEN) lexer->yylex();
			TYPES type2 = Expression();
			if(type1!=type2)
				Error("Meme type attendu ! ");
			cout<<"\tpop %rbx"<<endl;
			cout<<"\tcmpq %rdx,%rbx \t# On compare les deux valeurs pour voir la quelle est supèrieure(ou inférieure)"<<endl;
			cout<<"\tjge Max1"<<tag<<endl;
			cout<<"\tjmp Max2"<<tag<<endl;
			if(current!=LPARENT)
				Error("')' était attendu . ");		
		}
		else if(type1==DOUBLE){
			if(current!=COMMA)
				Error("',' était Attendu ! ");
			current=(TOKEN) lexer->yylex();
			TYPES type2 = Expression();
			if(type1!=type2)
				Error("Meme type attendu ! ");
			cout<<"\tpop %rbx"<<endl;
			cout<<"\tcmpq %rdx,%rbx \t# On compare les deux valeurs pour voir la quelle est supèrieure(ou inférieure)"<<endl;
			cout<<"\tjge Max1"<<tag<<endl;
			cout<<"\tjmp Max2"<<tag<<endl;
			if(current!=LPARENT)
				Error("')' était attendu . ");	
		}
		else
			Error("Type incorrecte ! ");
		cout<<"Max1"<<tag<<" : "<<endl;
		cout<<"\tpush %rbx"<<endl;
		cout<<"\tjmp FinMax"<<tag<<endl;
		cout<<"Max2"<<tag<<" : "<<endl;
		cout<<"\tpush %rdx"<<endl;
		cout<<"\tjmp FinMax"<<tag<<endl;
		cout<<"FinMax"<<tag<<" : "<<endl;
		current=(TOKEN) lexer->yylex();
		return type1;
	}	
	else
		Error("MAX était attendu . ");
}

// MinFonction := Expression { Expression; }
enum TYPES MinFonction(void){
	unsigned long long tag=TagNumber++;
	if(!strcmp(lexer->YYText(),"MIN")){
		cout<<"MINinit"<<tag<<" : "<<endl;
		current=(TOKEN) lexer->yylex();
		if(current!=RPARENT)
			Error("'(' était Attendu ! ");
		current=(TOKEN) lexer->yylex();
		TYPES type1 = Expression(); 
		cout<<"\tpop %rdx"<<endl;
		if(type1==INTEGER){
			if(current!=COMMA)
				Error("',' était Attendu ! ");
			current=(TOKEN) lexer->yylex();
			TYPES type2 = Expression();
			if(type1!=type2)
				Error("Meme type attendu ! ");
			cout<<"\tpop %rbx"<<endl;
			cout<<"\tcmpq %rdx,%rbx \t# On compare les deux valeurs pour voir la quelle est supèrieure(ou inférieure)"<<endl;
			cout<<"\tjge MIN1"<<tag<<endl;
			cout<<"\tjmp MIN2"<<tag<<endl;
			if(current!=LPARENT)
				Error("')' était attendu . ");		
		}
		else if(type1==DOUBLE){
			if(current!=COMMA)
				Error("',' était Attendu ! ");
			current=(TOKEN) lexer->yylex();
			TYPES type2 = Expression();
			if(type1!=type2)
				Error("Meme type attendu ! ");
			cout<<"\tpop %rbx"<<endl;
			cout<<"\tcmpq %rdx,%rbx \t# On compare les deux valeurs pour voir la quelle est supèrieure(ou inférieure)"<<endl;
			cout<<"\tjge MIN1"<<tag<<endl;
			cout<<"\tjmp MIN2"<<tag<<endl;
			if(current!=LPARENT)
				Error("')' était attendu . ");	
		}
		else
			Error("Type incorrecte ! ");
		cout<<"MIN1"<<tag<<" : "<<endl;
		cout<<"\tpush %rdx"<<endl;
		cout<<"\tjmp FinMIN"<<tag<<endl;
		cout<<"MIN2"<<tag<<" : "<<endl;
		cout<<"\tpush %rbx"<<endl;
		cout<<"\tjmp FinMIN"<<tag<<endl;
		cout<<"FinMIN"<<tag<<" : "<<endl;
		current=(TOKEN) lexer->yylex();
		return type1;
	}	
	else
		Error("MIN était attendu . ");
}

// ABS := Expression 
enum TYPES AbsFonction(void){
	if(!strcmp(lexer->YYText(),"ABS")){
		current=(TOKEN) lexer->yylex();
		if(current==RPARENT){					
			current=(TOKEN) lexer->yylex();
			TYPES type = Expression(); 
			if(type==DOUBLE){		
				
				cout << "\tfldl (%rsp)" << endl;
				cout << "\taddq $8, %rsp" << endl;
				cout << "\tfabs            \t# donnera la valuer absolue" << endl;
				cout << "\tsubq $8, %rsp" << endl;
				cout << "\tfstpl (%rsp)" << endl;
				if(current==LPARENT){
					current=(TOKEN) lexer->yylex();
					return type;
				}
				else
					Error("')' était attendu . ");
			
			}
			else
				Error("La Fonction ABS nécessite le type DOUBLE ! ");
		}
		else
			Error("'(' était attendu . ");
	}	
	else
		Error("ABS était attendu . ");
}

// SinFonction := Expression 
enum TYPES SinFonction(void){
	if(!strcmp(lexer->YYText(),"SIN")){
		current=(TOKEN) lexer->yylex();
		if(current==RPARENT){					
			current=(TOKEN) lexer->yylex();
			TYPES type = Expression(); 
			if(type==DOUBLE){		
				cout<<"\tpush RADIAN"<<endl;
				cout<<"\tfldl	8(%rsp)\t"<<endl;
				cout<<"\tfldl	(%rsp)\t# first operand(resultat de l'expression) -> %st(0) ; second operand -> %st(1)"<<endl;
				cout<<"\tfmulp	%st(0),%st(1)\t# %st(0) <- op1 * op2 ; %st(1)=null"<<endl;
				cout<<"\tfstpl 8(%rsp)"<<endl;
				cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl;
				
				cout<<"\tpop SIN"<<endl;
				cout<<"\tpush SIN"<<endl;
				
				cout << "\tfldl (%rsp)" << endl;
				cout << "\taddq $8, %rsp" << endl;
				cout << "\tfsin            \t# st(0) -> csin( st(0))" << endl;
				cout << "\tsubq $8, %rsp" << endl;
				cout << "\tfstpl (%rsp)" << endl;
				if(current==LPARENT){
					current=(TOKEN) lexer->yylex();
					return type;
				}
				else
					Error("')' était attendu . ");
			
			}
			else
				Error("La Fonction Sin nécessite le type DOUBLE ! ");
		}
		else
			Error("'(' était attendu . ");
	}	
	else
		Error("SIN était attendu . ");
}

// CosFonction := Expression
enum TYPES CosFonction(void){
	if(!strcmp(lexer->YYText(),"COS")){
		current=(TOKEN) lexer->yylex();
		if(current==RPARENT){					
			current=(TOKEN) lexer->yylex();
			TYPES type = Expression(); 
			if(type==DOUBLE){		
				cout<<"\tpush RADIAN"<<endl;
				cout<<"\tfldl	8(%rsp)\t"<<endl;
				cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
				cout<<"\tfmulp	%st(0),%st(1)\t# %st(0) <- op1 * op2 ; %st(1)=null"<<endl;
				cout<<"\tfstpl 8(%rsp)"<<endl;
				cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl;
				
				cout<<"\tpop COS"<<endl;
				cout<<"\tpush COS"<<endl;
				
				cout << "\tfldl (%rsp)" << endl;
				cout << "\taddq $8, %rsp" << endl;
				cout << "\tfcos            \t# st(0) -> cos( st(0))" << endl;
				cout << "\tsubq $8, %rsp" << endl;
				cout << "\tfstpl (%rsp)" << endl;
				if(current==LPARENT){
					current=(TOKEN) lexer->yylex();
					return type;
				}
				else
					Error("')' était attendu . ");
			
			}
			else
				Error("La Fonction COS nécessite le type DOUBLE ! ");
		}
		else
			Error("'(' était attendu . ");
	}	
	else
		Error("COS était attendu . ");
}

// SqrtFonction := Expression
enum TYPES SqrtFonction(void){
	if(!strcmp(lexer->YYText(),"SQRT")){
		current=(TOKEN) lexer->yylex();
		if(current==RPARENT){					
			current=(TOKEN) lexer->yylex();
			TYPES type = Expression(); 
			if(type==DOUBLE){		
				cout << "\tfldl (%rsp)" << endl;
				cout << "\taddq $8, %rsp" << endl;
				cout << "\tfsqrt           \t# st(0) -> sqrt( st(0))" << endl;
				cout << "\tsubq $8, %rsp" << endl;
				cout << "\tfstpl (%rsp)" << endl;
				if(current==LPARENT){
					current=(TOKEN) lexer->yylex();
					return type;
				}
				else
					Error("')' était attendu . ");
			
			}
			else
				Error("La Fonction SQRT nécessite le type DOUBLE ! ");
		}
		else
			Error("'(' était attendu . ");
	}	
	else
		Error("SQRT était attendu . ");
}

//SqrFonction := Expression 
enum TYPES SqrFonction(void){
	if(!strcmp(lexer->YYText(),"SQR")){
		current=(TOKEN) lexer->yylex();
		
		if(current==RPARENT){
			current=(TOKEN) lexer->yylex();
			
			TYPES type=Expression();

			if(type==INTEGER){
				cout<<"\tpop  SQR_INTEGER"<<endl;
				cout << "\tpush SQR_INTEGER"<<endl;
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout<<"\tpush SQR_INTEGER"<<endl;
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rbx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
			}
			else if(type==DOUBLE){
				cout<<"\tpop SQR_DOUBLE"<<endl;	
				cout<<"\tpush SQR_DOUBLE"<<endl;
				cout<<"\tpop SQR_DOUBLE2"<<endl;
				cout<<"\tpush SQR_DOUBLE"<<endl;
				cout<<"\tpush SQR_DOUBLE2"<<endl;
				cout<<"\tfldl	8(%rsp)\t"<<endl;
				cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
				cout<<"\tfmulp	%st(0),%st(1)\t# %st(0) <- op1 * op2 ; %st(1)=null"<<endl;
				cout<<"\tfstpl 8(%rsp)"<<endl;
				cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl;
			} 
			else
				Error("type pas correct pour le square");	
			
			if(current==LPARENT){
				current=(TOKEN) lexer->yylex();
				return type;
			}
			else
				Error("')' était attendu");
		}
		else
			Error("'(' était attendu");		
	}
	else 
		Error("SQR Attendue");
}

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	enum TYPES type1, type2;
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	type1=DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	type2=Expression();
	if(type2!=type1) Error("Attention! Meme type attendu.");
	if(type1==CHAR){
		cout << "\tpop %rax"<<endl;
		cout << "\tmovb %al,"<<variable<<endl;
	}
	else if(y3==1){
		int b=indice_type[y4];
		array[y4][b]=variable;
		y3=0;//si y3=1 alors on va affecter un array entier à une variable et on pourra plus l'utiliser. Dans ce case on fera pas de pop mais je garderais le nom de la variable dans un tableau pour pouvoir faire en display en suite. La verification du type a été deja faite avanat à travers le factor. 
		y4=0;
	}
	else cout << "\tpop "<<variable<<endl;
	if (n<255){
		var[n]=variable;//On met dans le tableau l'id de la variable pour la recuperer ensuite
		n++;//On augmente la taille du tableau
	}
	if (nb<255){
		typ[nb]=type1;//On met dans le tableau le type du dernier element du tableau var
		nb++;//On augmente la taille du tableau
	}
}

//IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void){
	unsigned long long tag=TagNumber++;
	if(!strcmp(lexer->YYText(),"IF")){
		current=(TOKEN) lexer->yylex();
		TYPES expression=Expression();
		if(expression!=BOOLEAN)
			Error("Attention! Boolean attendue.");
		
		cout<<"\tpop %rax\t# Prend le resultat de l'expression"<<endl;
		cout<<"\tcmpq $0,%rax"<<endl;
		cout<<"\tje Else"<<tag<<" \t# si c'est egal a 0 on saute a else"<<endl;
		
		if(!strcmp(lexer->YYText(),"THEN")){
			current=(TOKEN) lexer->yylex();
			Statement();
			cout<<"\tjmp Next"<<tag<<" \t# On continue et on va pas dans Else"<<endl;
			cout<<"Else"<<tag<<":"<<endl;
			if(!strcmp(lexer->YYText(),"ELSE")){
				current=(TOKEN) lexer->yylex();
				Statement();
			}
		}	
		else
			Error("THEN Attendue");
	}
	else 
		Error("IF Attendue");
	cout<<"Next"<<tag<<":"<<endl;
}
				
//WhileStatement := "WHILE" Expression DO Statement
void WhileStatement(void){
	unsigned long long tag=TagNumber++;
	cout<<"While"<<tag<<":"<<endl;
	if(!strcmp(lexer->YYText(),"WHILE")){
		current=(TOKEN) lexer->yylex();

		TYPES expression=Expression();
		if(expression!=BOOLEAN)
			Error("Attention! Boolean attendue.");
		
		cout<<"\tpop %rax\t# Prend le resultat de l'expression"<<endl;
		cout<<"\tcmpq $0, %rax"<<endl;
		cout<<"\tje EndWhile"<<tag<<"\t# si c'est egal a zero on sort de la boucle"<<tag<<endl;

		if(!strcmp(lexer->YYText(),"DO")){
			current=(TOKEN) lexer->yylex();
			Statement();
			
			cout<<"\tjmp While"<<tag<<" \t# on revien au debut de la boucle"<<endl;
			cout<<"EndWhile"<<tag<<" : \t# on termine le while"<<endl;

		}	
		else
			Error("DO Attendue");
	}
	else 
		Error("WHILE Attendue");
}

void DisplayArray(int i,int j){
	current=(TOKEN) lexer->yylex();
	if(current!=NUMBER) Error("Un numero était attendu !");
	string indice=lexer->YYText();
	int ind=atoi(indice.c_str());
	if(ind<=0 or ind>=j-1) Error("Indice Introuvable ! Verifier si ce array a été bien affecté à une variable ");
	current=(TOKEN) lexer->yylex();
	if(current!=LBRACKET) Error("']' était attendu !");
	current=(TOKEN) lexer->yylex();
	if(array[i][j-1]=="INTEGER"){
		cout << "\tpush $"<<array[i][ind]<<endl;
		cout << "\tpop %rsi\t# The value to be displayed"<<endl;
		cout << "\tmovq $FormatString1, %rdi\t# \"%llu\\n\""<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	printf@PLT"<<endl;
	}
	else if(array[i][j-1]=="DOUBLE"){
		unsigned int* i2;
		double d;
		string indice=array[i][ind];	
		d=atof(indice.c_str());
		i2=(unsigned int *) &d; 
		cout <<"\tsubq $8,%rsp\t\t\t# allocate 8 bytes on stack's top"<<endl;
		cout <<"\tmovl	$"<<*i2<<", (%rsp)\t# Conversion of "<<d<<" (32 bit high part)"<<endl;
		cout <<"\tmovl	$"<<*(i2+1)<<", 4(%rsp)\t# Conversion of "<<d<<" (32 bit low part)"<<endl;
		cout << "\tmovsd	(%rsp), %xmm0\t\t# &stack top -> %xmm0"<<endl;
		cout << "\tsubq	$16, %rsp\t\t# allocation for 3 additional doubles"<<endl;
		cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
		cout << "\tmovq $FormatString2, %rdi\t# \"%lf\\n\""<<endl;
		cout << "\tmovq	$1, %rax"<<endl;
		cout << "\tcall	printf"<<endl;
		cout << "nop"<<endl;
		cout << "\taddq $24, %rsp\t\t\t# pop nothing"<<endl;
	}
	else
		Error("Type incorrecte ! ");
}

//DisplayStatement := "DISPLAY" Expression
void DisplayStatement(void)
{
	unsigned long long tag=++TagNumber;
	if(!strcmp(lexer->YYText(),"DISPLAY")){
		current=(TOKEN) lexer->yylex();
		
		int a=0;//si on fait le display d'un array on s'arretera.
		//On Verifie si on veut faire le Display d'un elelement d'un array
		for(int i=0;i<y1;i++){
			for(int j=0;j<255;j++){
				if(!strcmp(lexer->YYText(),array[i][j].c_str())){
					current=(TOKEN) lexer->yylex();
					if(current!=RBRACKET) Error("'[' était attendu pour indiquer l indice de l'element à afficher . ");
					DisplayArray(i,j);//On pourra voir un seuele element d'une array avec l'indice indiqué.
					a++;//Arreter la fonction
				}
			}
		}
		
		if(a==0){
			cout<<"Display"<<tag<<":"<<endl;
			TYPES exp=Expression();

			if(exp==INTEGER){
				cout << "\tpop %rsi\t# The value to be displayed"<<endl;
				cout << "\tmovq $FormatString1, %rdi\t# \"%llu\\n\""<<endl;
				cout << "\tmovl	$0, %eax"<<endl;
				cout << "\tcall	printf@PLT"<<endl;
			}
			else if(exp==BOOLEAN){
				cout << "\tpop %rdx\t# Zero : False, non-zero : true"<<endl;
				cout << "\tcmpq $0, %rdx"<<endl;
				cout << "\tje False"<<tag<<endl;
				cout << "\tmovq $TrueString, %rdi\t# \"TRUE\\n\""<<endl;
				cout << "\tjmp Next"<<tag<<endl;
				cout << "False"<<tag<<":"<<endl;
				cout << "\tmovq $FalseString, %rdi\t# \"FALSE\\n\""<<endl;
				cout << "Next"<<tag<<":"<<endl;
				cout << "\tcall	puts@PLT"<<endl;
			}
			else if(exp==DOUBLE){
				cout << "\tmovsd	(%rsp), %xmm0\t\t# &stack top -> %xmm0"<<endl;
				cout << "\tsubq	$16, %rsp\t\t# allocation for 3 additional doubles"<<endl;
				cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
				cout << "\tmovq $FormatString2, %rdi\t# \"%lf\\n\""<<endl;
				cout << "\tmovq	$1, %rax"<<endl;
				cout << "\tcall	printf"<<endl;
				cout << "nop"<<endl;
				cout << "\taddq $24, %rsp\t\t\t# pop nothing"<<endl;
			}
			else if(exp==CHAR){
				cout<<"\tpop %rsi\t\t\t# get character in the 8 lowest bits of %si"<<endl;
				cout << "\tmovq $FormatString3, %rdi\t# \"%c\\n\""<<endl;
				cout << "\tmovl	$0, %eax"<<endl;
				cout << "\tcall	printf@PLT"<<endl;
			}
			else Error("DISPLAY ne fonctionne pas pour ce type de donnée.");
		}
	}
	else
		Error("DISPLAY attendu ! ");
}	

//ForStatement := "FOR" AssignementStatement "To" Expression "DO" Statement
void ForStatement(void){
	enum TYPES type1, type2;
	unsigned long long tag=TagNumber++;
	cout<<"ForInit"<<tag<<" : "<<endl;
	if(!strcmp(lexer->YYText(),"FOR")){
		current=(TOKEN) lexer->yylex();
		AssignementStatement();
		type1=typ[nb-1];
		if(!strcmp(lexer->YYText(),"To")){
			current=(TOKEN) lexer->yylex();
			
			cout<<"For"<<tag<<" : "<<endl;
			
			type2=Expression();
			
			if(type1==type2){
				cout<<"\tpop %rax\t# Prend le resultat de l'expression"<<endl;
				cout<<"\tcmpq "<<var[n-1]<<", %rax"<<endl;
				cout<<"\tjnae EndFor"<<tag<<" \t# si c'est egal a zero on sort de la boucle"<<endl;//En effet si il la variable plus grand que la variable aprés le To la boucle tournera à l'infini si on met le je. Donc on met le jnae.
				
				if(!strcmp(lexer->YYText(),"DO")){
					if(type1==INTEGER){
						current=(TOKEN) lexer->yylex();
						cout<<"\taddq $1," <<var[n-1]<<endl;
						n--;
						Statement();
					}
					if(type1==DOUBLE){
						current=(TOKEN) lexer->yylex();
						cout<<"\tpush "<<var[n-1]<<endl;	
						cout<<"\tpush FOR_DOUBLE"<<endl;//qui contient la valeur 1.0. 
						//On pourrais utliser aussi la commande fld1 quirajoute 1.0 au stack's top
						cout<<"\tfldl	8(%rsp)\t"<<endl;
						cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
						cout<<"\tfaddp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
						cout<<"\tfstpl 8(%rsp)"<<endl;
						cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl;
						cout<<"\tpop "<<var[n-1]<<endl;	
						n--;
						Statement();
					} 
					cout<<"\tjmp For"<<tag<<" \t# on revien au debut du For"<<endl;
					cout<<"EndFor"<<tag<<" : \t# on termine le For"<<endl;
					cout<<"\tjmp EndForInit"<<tag<<" \t# on termine completement le For"<<endl;
					cout<<"EndForInit"<<tag<<" : \t# on termine le For completement"<<endl;	
				}
				else
					Error("DO Attendue");
			}
			else
				Error("Attention! La variable après le FOR et aprés le TO doivent avoir le meme type. ");
		}	
		else
			Error("To Attendue");
	}
	else 
		Error("FOR Attendue");
}

//BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void){
	if(!strcmp(lexer->YYText(),"BEGIN")){
		current=(TOKEN) lexer->yylex();
		Statement();
		while(!strcmp(lexer->YYText(),";"))
		{
			current=(TOKEN) lexer->yylex();
			Statement();
		}
		if(!strcmp(lexer->YYText(),"END"))
			current=(TOKEN) lexer->yylex();
		else
			Error("END Attendue");
		}	
	else 
		Error("BEGIN Attendue");
}

//<case label list> ::= <expression> {, <expression> }
enum TYPES CaseLabelList(unsigned long long tag){
	TYPES type1=Expression();
	cout<<"\tpop %rax"<<endl;;
	cout<<"\tcmpq %rbx,%rax"<<endl;;
	cout<<"\tje CaseStatement"<<tag<<endl;
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		TYPES type2=Expression();
		if(type1!=type2)
			Error("Meme type attendu ! ");
		cout<<"\tpop %rax"<<endl;;
		cout<<"\tcmpq %rbx,%rax"<<endl;;
		cout<<"\tje CaseStatement"<<tag<<endl;
	}
	cout<<"\tjmp FinCasestatement"<<tag<<endl;
	return type1;
}

//<case list element> ::= <case label list> : <statement>
enum TYPES CaseListElement(unsigned long long tag,unsigned long long tag2){
	TYPES type=CaseLabelList(tag);
	if(current==COLON){
		cout<<"CaseStatement"<<tag<<" : "<<endl;
		current=(TOKEN) lexer->yylex();
		Statement();
		cout<<"\tjmp FinCase"<<tag2<<endl;
		cout<<"FinCasestatement"<<tag<<" : "<<endl;
	}
	else 
		Error("COLON attendu ! ");
	return type;
}
	
	
//<case statement> ::= "CASE" <expression> "OF" <case list element> {; <case list element> }
void CaseStatement(void){
	unsigned long long tag=TagNumber++;
	unsigned long long tag2=tag;//On fais un deuxieme tag Pour les jump et pour pas confondre les differentas numero du tag 
	//En effet le premier tag augmentera et on aura donc besoin d' un second tag pour retourner à la fin du Case creè dans cette fonction
	cout<<"Case"<<tag<<" : "<<endl;
	if(!strcmp(lexer->YYText(),"CASE"))
	{
		current=(TOKEN) lexer->yylex();
		TYPES type1=Expression();
		cout<<"\tpop %rbx"<<endl;
		if(!strcmp(lexer->YYText(),"OF")){
			current=(TOKEN) lexer->yylex();
			tag++;
			TYPES type2=CaseListElement(tag,tag2);
			tag++;
			if(type1==type2){
				while(current==SEMICOLON){
					current=(TOKEN) lexer->yylex();
					type2=CaseListElement(tag,tag2);
					tag++;
					if(type1!=type2)
						Error("Meme type attendu ! ");
				}
				if(!strcmp(lexer->YYText(),"END"))
				{
					cout<<"FinCase"<<tag2<<" : "<<endl;
					current=(TOKEN) lexer->yylex();
				}
				else
					Error("'END' Attendu ! ");
			}
			else
				Error("Meme type attendu ! ");
		}
		else
			Error("'OF' était attendu .");
	}
	else
		Error("'CASE' était attendu .");		
}	

// RandomStatement := DeclaredVariable
void RandomStatement(void){
	if(!strcmp(lexer->YYText(),"Randomize")){
		int a=rand() % 1000 + 1;//Valuer random entre1 et 1000(On pourrais aussi rien mettre pour faire des nombres sans limites
		cout <<"\tpush $"<<a<<endl;
		cout<<"\tpop Random"<<endl;
		DeclaredVariables["Random"]=INTEGER;//On declare la variable Random si Randomize a ètè inserè et on met son type,cad INTEGER 
	}
	else
		Error("'Randomize' attendu ! ");
}

//Called by AssignementStatement ssi le nom de la procedure existe
void ProcedureStatement(int i){
	cout<<"\tcall Procedure"<<fonction[i]<<endl;
	current=(TOKEN) lexer->yylex();
}

//ProcedureStatement := <ID> ; BEGIN BlockStatement
void ProcedureDeclaration(void){
	string nom_procedure;
	if(!strcmp(lexer->YYText(),"PROCEDURE")){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("ID était attendu ! ");
		for(int i=0;i<x;i++){
			if(!strcmp(lexer->YYText(),fonction[i].c_str()))//pour convertir un string to const char*
				Error("Deux Procedures ne peuvent pas avoir le meme nom ! ");
		}
		nom_procedure=lexer->YYText();
		fonction[x]=nom_procedure;
		x++;
		cout<<"Procedure"<<fonction[x-1]<<" : "<<endl;//Cette partie sera appellè quand on mettra un nom d'une procedure précédement creè. On utilisera \tcall
		current=(TOKEN) lexer->yylex();
		if(current!=SEMICOLON)
			Error("';' attendu à la fin d'une procedure . ");
		current=(TOKEN) lexer->yylex();
		if(!strcmp(lexer->YYText(),"BEGIN")){
			BlockStatement();
			cout<<"\tret "<<endl;//Le block procedure s'arretera et le mein reprendra où il s'était arreté
		}
		else
			Error("'BEGIN' attendu ! ");
	}
	else
		Error("'PROCEDURE' attendu ! ");
}
			
//Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement | DisplayStatement | CaseStatement | RandomStatement.
void Statement(void){
	if(current==ID){
		int procedure=0;//Si une procedure existe on fait pas l'AssignementStatement
		for(int i=0;i<x;i++){
			if(!strcmp(lexer->YYText(),fonction[i].c_str())){
				ProcedureStatement(i);//On met en argument le i pour faire la procedure correcte
				procedure ++;
			}
		}
		if(procedure==0)
			AssignementStatement();
	}
	else
	{
		if(current==KEYWORD)
			{
				if(!strcmp(lexer->YYText(),"WHILE"))
					WhileStatement();
				else if(!strcmp(lexer->YYText(),"FOR"))
					ForStatement();
				else if(!strcmp(lexer->YYText(),"IF"))
					IfStatement();
				else if(!strcmp(lexer->YYText(),"BEGIN"))
					BlockStatement();
				else if(!strcmp(lexer->YYText(),"DISPLAY"))
					DisplayStatement();
				else if(!strcmp(lexer->YYText(),"CASE"))
					CaseStatement();
				else if(!strcmp(lexer->YYText(),"Randomize")){
					RandomStatement();
					current=(TOKEN) lexer->yylex();
				}
			}
		else
			Error("instruction attendue");

	}
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.align 8"<<endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
	{
		Error("caractère '.' attendu");
	}
	current=(TOKEN) lexer->yylex();
}

// Program := [VarDeclarationPart] StatementPart
void Program(void){
		if(current==KEYWORD and strcmp(lexer->YYText(),"TYPE")==0){
			TypeDeclaration();
			while(current==SEMICOLON)
			{
				TypeDeclaration();
			}
			if(current!=DOT)
				Error("'.' attendu");
			current=(TOKEN) lexer->yylex();
		}
		if(current==KEYWORD and strcmp(lexer->YYText(),"VAR")==0) 
			VarDeclarationPart();
		while(current==KEYWORD and strcmp(lexer->YYText(),"PROCEDURE")==0){ //Une procedure sera donc declarée aprés le VAR et non pas dans le main. On utilise donc un while pour faire plusieres procedures
			ProcedureDeclaration();
			if(current!=SEMICOLON)
				Error("';' attendu à la fin d'une declaration de fonction . ");
			current=(TOKEN) lexer->yylex();
		}
	StatementPart();	
}

int main(void){	
	for(int i=0;i<255;i++)
	{
		array[i]=new string[255];
	}
	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by Lorenzo La Mura"<<endl;
	cout << ".data"<<endl;
	cout << "FormatString1:\t.string \"%llu\"\t# used by printf to display 64-bit unsigned integers"<<endl; 
	cout << "FormatString2:\t.string \"%lf\"\t# used by printf to display 64-bit floating point numbers"<<endl; 
	cout << "FormatString3:\t.string \"%c\"\t# used by printf to display a 8-bit single character"<<endl; 
	cout << "TrueString:\t.string \"TRUE\"\t# used by printf to display the boolean value TRUE"<<endl; 
	cout << "FalseString:\t.string \"FALSE\"\t# used by printf to display the boolean value FALSE"<<endl; 
	
	/*Faire attention aux nom des variable ci-dessous car dés fois j'utiliserais les memes variables pour des fonction
	differentes mais avec le meme besoin pour pas créer trop de variables meme s'ils existent d'autre façon moins
	"moches" de réaliser ces fonctions.     
	Variable : */

	cout << "FOR_DOUBLE:	.double 1.0"<<endl;//Pour rajouter 1.0 dans le for si i est en double. On Peut utiliser aussi la commande fld1.
	
	cout << "POW_INTEGER:	.quad 0"<<endl;//Pour le power, pour guarder le premier nombre.
	cout << "POW_DOUBLE:	.double 0.0"<<endl;//Pour le power, pour guarder le premier nombre.
	
	cout << "SQR_INTEGER:	.quad 0"<<endl;//Pour faire le carrè. En effet on met une valeur x dans cette variable pour le multiplier par lui meme
	cout << "SQR_DOUBLE:	.double 0.0"<<endl;//Pareil mais avec un double.D'abord on fait un pop du resultat de l'expression dans cette variable
	cout << "SQR_DOUBLE2:	.double 0.0"<<endl;//En suite on recopie cette meme valeur sur cette deuxieme variable pour les multiplier entre eux
	
	cout << "RADIAN: 	.double 0.017453292"<<endl; //radian=x*pigrec/180 où x sera defini dans la fonction SIN ou COS
	cout << "SIN: 	.double 0.0"<<endl;//Pour stocker le resultat de l'expression
	cout << "COS: 	.double 0.0"<<endl;//Pour stocker le resultat de l'expression
	
	cout << "Random:	.quad 0"<<endl;//Variable Random declarè seulement si on ecrit 'Randomizer' (voir RandomStatement)
	
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			






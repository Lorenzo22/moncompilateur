# Lorenzo La Mura Gr 4 Moncompilateur

**Attention!**
	Dans le main() j'ai crée des variables par exemple 'FOR_DOUBLE:	.double 1.0' (meme si en ce cas je pouvais utiliser la commande assembleur fld1). En effet j'utilise ces variables (des fois la meme dans des fonctions differents) pour stocker des push donnant le resultat des expression etc. Je sais qu'il y a aussi d'autre façon de le faire mais sur le moment il me semblait la chose la plus simple à faire. J'ai verifié et ces variable ne causent pas de probléms, car tout marche correctment. Je comprend que ça peut etre difficile à lire et à comprendre car j'utilise le FOR_DOUBLE qui vaut 1.0 meme dans d'autre fonction comme par exemple SUCC pour la meme utilité. Lire les commaintaires pour comprendre mieux les fonctions, surtout pour les arrays et les procedures. Beaucoup de fonction sont incomplete parce que il fallais trop de temps pour tout implemanter, mais dans l'ensemble avec les bonnes correction tout marche correctement. Utiliser le test.p pour comprendre toutes les fonctionnalités du compilateur.

# Fonctions réalisés en plus des TPS :

**FACT (pas présent sur Pascal) :** Donne le factorielle d'un entier. (ex: a:=FACT(5+6); [cad 11!] )

**POW (pas présent sur Pascal):** Donne la pussinace d'un nombre entier ou double. Aprés la paranthese on met d'abord le nombre à utiliser et aprés une virgule suivie de l'exposant(nombre entier). (ex: a:=POW(6+5,1+1);  [cad 11^2])

**MAX/MIN (pas présent sur Pascal):** Donne la valeure maximale ou minimale entre deux nombres doubles ou intiers separès par une virgule en faisant des comparaison.(ex : a:=MAX(5,3); )

**LENGTH :** Donne la longueur d'un array (l'array lui meme ou une variable à laquelle un array a été affecté). C'est à dire combien d'element il y a dans cet array. Le retour sera forcement un INTEGER. (ex : a:=LENGTH(nom_array); )

**SQR :** Donne le carrè d'un nombre soit entier soit double (ex : a:=SQR(5+3); )

**SQRT :** Donne la racine carré seulement d'un nombre double car on utilise une commande assembleur propre aux flottant c'est à dire fsqrt (ex : a:=SQRT(5.0+3.0); )

**COS/SIN :** Donne le cos et le sin seulement d'un nombre double car on utilise une commande assembleur propre aux flottant c'est à dire fcos et fsin 
(ex : a:=COS(5.0+3.0); )

**ABS :** Donne la valeur absolue seulement d'un nombre double car on utilise une commande assembleur propre aux flottant c'est à dire fabs (ex : a:=ABS(3.0-5.0); )
En effet une variable ne pourra jamais etre declaré negatif en faisant par exemple a:=-2 Mais on pourra faire a:=3-5.

**SUCC/PRED :** Donne la valeur successive ou precedente d'un nombre entier ou double en rajoutant ou enlevant 1 à la variable.(ex : a:=SUCC(5.0+3.0); )

**Randomize/Random :** En ecrivant 'Randomize' à n'importe quel moment aprés la déclaration on pourra utiliser Random comme si c'était un numero donné par hasard. Random sera declaser comme une variable avec la fonction DeclaredVariable[Random]=INTEGERS une fois que Randomizer sera présent dans le test.p. Sa valeure sera un entier et sera toujours pareil jusqu'à quand on recompilera pas le compilateur.cpp car ce nombre est donné par une fonction en c++. En effet je suis pas arrivè à trouver une commande assembleur qui permet de prendre un nombre aléatoire qui change à chaque fois. Donc on pourra l'utiliser que avec les entiers. 
(ex : a:=Random+5; )

# Statements réalisés en plus des TPS :

**ARRAY :** C'est la fonction la plus compliquè à faire selon moi. En faite dans Program j'ai mis que si avant la VAR on tape TYPE on peut initialiser des arrays contenant des entiers ou des doubles (que des numeros, pas d'expression, et pas de char car j'ai eu des problémes de segmentation avec) separés par des point virgules et terminé avec un point. EX : "TYPE nom_Array ARRAY[1, 2, ..] OF INTEGER. VAR ...." Exactement comme en pascal. 
Pour appeller ces tableau il faut forcement l'affectè à une variable du meme type. J'ai crée deux façon pour affecté cette array. Par exemple si on veut affecter une seulr valeur d'un array à travers son indice (qui commence par 1 et non pas de 0) en faisant par exemple p:=nom_Array[3] +SIN(5); p aura comme pop la valeur de l'indice 3 du tableau + sin(5) et donc on pourra faire ce qu'on veut avec p. Mais il faut un espace entre ']' et '+'.Par contre on peut aussi affecter un array entier à une variable en faisant par exemple p:=nom_Array; mais en ce cas on pourra seulement faire un DISPLAY de p mais on pourra pas l'utiliser car j'ai pas eu assez de temps pour modifier tous les fonctions(il fallais modifier expression etc). Si on utilise cette derniere methode on ne pourra pas affecter le meme tableau pour des variables differentes mais une pour une seule variable. Pour faire le display de un array on dois marquer par exemple 
DISPLAY p[indicequ_on_veut_afficher]; si non un erreur s'affichera. Pour guarder tous les valeurs des arrays etc j'ai utilisé un tableau multidimensionnel avec beaucoup d'indice pour m'aider dans la recherche d'informations. Tout est marqué dans les commaintaires.

**FOR :** Le FOR marchera soit avec les intiers soit avec les double en rajoutant 1 ou 1.0 à la variable avant le TO (FOR i To). Pour recuperer le nom et le type de cette variable j'ai crée un tableau de string et de Types qui dans le AssignementStatement. Le for imbriqué marchera donc parfaitement. 

**CASE :** Je suivi un peu le meme plan donné en cour mais j'ai pas inséré la fonction empty car j'ai pas trouvè d'utilité. En effet on ne peut pas faire une case vide en pascal. Le case terminera quand on mettra pas de poin virgule car chaque fois qu'il y en a le programme il attend un autre case.

**PROCEDURE :** Une procedure sera declarée seulement avant le main et pour l'appeller suffira de taper le nom de la procedure (ex :nom_procedure; ) Le Statement verifiquera s'il existe une procedure pour le ID courrant et si c'est le cas avec la commande assembleur call la fonction sera effectuée. La declaration d'une procedure sera fais dans la fonction Program.

# Commandes :

**Download the repository :**

> git clone ggit@framagit.org:Lorenzo22/moncompilateur.git

**Build the compiler and test it :**

> make test

**Have a look at the output :**

> gedit test.s

**Debug the executable :**

> ddd ./test

**Commit the new version :**

> git commit -a -m "What's new..."

**Send to your framagit :**

> git push -u origin master

**Get from your framagit :**

> git pull -u origin master

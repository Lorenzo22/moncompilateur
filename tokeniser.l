%{
// This is our Lexical tokeniser 
// It should be compiled into cpp with :
// flex++ -d -otokeniser.cpp tokeniser.l 
// And then compiled into object with
// g++ -c tokeniser.cpp
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string.

#include "tokeniser.h"
#include <iostream>

using namespace std;

%}

%option noyywrap
%option c++
%option yylineno

charconst	\'\\?.\'
ws      [ \t\n\r]+
alpha   [A-Za-z]
digit   [0-9]
number  {digit}+(\.{digit}+)?
keyword	(IF|WHILE|FOR|THEN|ELSE|To|DO|BEGIN|END|PROCEDURE|DISPLAY|SUCC|PRED|FACT|POW|Randomize|MAX|MIN|SQR|SIN|COS|ABS|SQRT|CASE|OF|VAR|TYPE|LENGTH|INTEGER|BOOLEAN|CHAR|DOUBLE)
id	{alpha}({alpha}|{digit})*
addop	(\+|\-|\|\|)
mulop	(\*|\/|%|\&\&)
relop	(\<|\>|"=="|\<=|\>=|!=)
unknown [^\"A-Za-z0-9 \n\r\t\(\)\<\>\=\!\%\&\|\}\-\;\.]+

%%

{addop}		return ADDOP;
{mulop}		return MULOP;
{relop}		return RELOP;
{number}	return NUMBER;
{keyword}	return KEYWORD;
{id}		return ID;
{charconst}	return CHARCONST;
"["		return RBRACKET;
"]"		return LBRACKET;
","		return COMMA;
":"		return COLON;
"="		return EGAL;
";"		return SEMICOLON;
"."		return DOT;
":="	return ASSIGN;
"("		return RPARENT;
")"		return LPARENT;
"!"		return NOT;
<<EOF>>		return FEOF;
{ws}    {/* skip blanks and tabs */};
"(*"    { /* Skip comments between '(*' and '*)' */
		int c;
		while((c = yyinput()) != 0){
     			if(c == '*'){
     	    			if((c = yyinput()) == ')')
    	        			break;
    	     			else
  	          			unput(c);
  	   		}	
		}
	};

{unknown}	return UNKNOWN;

%%

